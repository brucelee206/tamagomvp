//
//  FirstViewController.m
//  tamagoMVP
//
//  Created by Tung Dao on 6/28/17.
//  Copyright © 2017 Tung Dao. All rights reserved.
//

#import "FirstViewController.h"
#import <WowzaGoCoderSDK/WowzaGoCoderSDK.h>

@interface FirstViewController () <WZStatusCallback>

@property (nonatomic, strong) WowzaGoCoder *goCoder;
@property (weak, nonatomic) IBOutlet UIButton *broadcastButton;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Register the GoCoder SDK license key
    NSError *goCoderLicensingError = [WowzaGoCoder registerLicenseKey:@"GOSK-DF43-0103-E273-F11F-A8F3"];
    if (goCoderLicensingError != nil) {
        // Log license key registration failure
        NSLog(@"%@", [goCoderLicensingError localizedDescription]);
    } else {
        // Initialize the GoCoder SDK
        self.goCoder = [WowzaGoCoder sharedInstance];
    }
    
    if (self.goCoder != nil) {
        // Associate the U/I view with the SDK camera preview
        self.goCoder.cameraView = self.view;
        
        // Start the camera preview
        [self.goCoder.cameraPreview startPreview];
    }
    
    // Get a copy of the active config
    WowzaConfig *goCoderBroadcastConfig = self.goCoder.config;
    
    // Set the defaults for 720p video
    [goCoderBroadcastConfig loadPreset:WZFrameSizePreset1280x720];
    
    // Set the connection properties for the target Wowza Streaming Engine server or Wowza Cloud account
    goCoderBroadcastConfig.hostAddress = @"54.169.255.206";
    goCoderBroadcastConfig.portNumber = 1935;
    goCoderBroadcastConfig.applicationName = @"live";
    goCoderBroadcastConfig.streamName = @"myStream";
    
    // Update the active config
    self.goCoder.config = goCoderBroadcastConfig;
    
//    [self.broadcastButton addTarget:self action:@selector(broadcastButtonTapped:) forControlEvents: UIControlEventTouchUpInside];
}
- (IBAction)broadcastButtonTapped:(id)sender {
    // Ensure the minimum set of configuration settings have been specified necessary to
    // initiate a broadcast streaming session
    NSError *configValidationError = [self.goCoder.config validateForBroadcast];
    
    if (configValidationError != nil) {
        UIAlertView *alertDialog =
        [[UIAlertView alloc] initWithTitle:@"Incomplete Streaming Settings"
                                   message: self.goCoder.status.description
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alertDialog show];
    } else if (self.goCoder.status.state != WZStateRunning) {
        // Start streaming
        [self.goCoder startStreaming:self];
    }
    else {
        // Stop the broadcast that is currently running
        [self.goCoder endStreaming:self];
    }
}

- (void) onWZStatus:(WZStatus *) goCoderStatus {
    // A successful status transition has been reported by the GoCoder SDK
    NSString *statusMessage = nil;
    
    switch (goCoderStatus.state) {
        case WZStateIdle:
            statusMessage = @"The broadcast is stopped";
            break;
            
        case WZStateStarting:
            statusMessage = @"Broadcast initialization";
            break;
            
        case WZStateRunning:
            statusMessage = @"Streaming is active";
            break;
            
        case WZStateStopping:
            statusMessage = @"Broadcast shutting down";
            break;
    }
    
    if (statusMessage != nil)
        NSLog(@"Broadcast status: %@", statusMessage);
}

- (void) onWZError:(WZStatus *) goCoderStatus {
    // If an error is reported by the GoCoder SDK, display an alert dialog
    // containing the error details using the U/I thread
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertDialog =
        [[UIAlertView alloc] initWithTitle:@"Streaming Error"
                                   message:goCoderStatus.description
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alertDialog show];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
