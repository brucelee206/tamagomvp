//
//  AppDelegate.h
//  tamagoMVP
//
//  Created by Tung Dao on 6/28/17.
//  Copyright © 2017 Tung Dao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

