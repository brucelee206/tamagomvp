//
//  main.m
//  tamagoMVP
//
//  Created by Tung Dao on 6/28/17.
//  Copyright © 2017 Tung Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
